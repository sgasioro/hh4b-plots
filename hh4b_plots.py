import matplotlib.pyplot as plt
from phys_utils import *


from matplotlib.ticker import AutoLocator, AutoMinorLocator, MultipleLocator
import matplotlib.font_manager as font_manager
import logging
from matplotlib import rcParams

from matplotlib import font_manager

font_dirs = ["./fonts/freesans/"]
font_files = font_manager.findSystemFonts(fontpaths=font_dirs)

for font_file in font_files:
    font_manager.fontManager.addfont(font_file)

import logging
logging.getLogger('matplotlib').setLevel(logging.ERROR)

plt.style.use('stylesheets/ATLAS.mplstyle')

from colors import *

def mid(bins):
    return np.array([bins[i]+(bins[i+1]-bins[i])/2. for i in range(len(bins)-1)])

def widths(bins):
    return np.array([bins[i+1]-bins[i] for i in range(len(bins)-1)])

#Types of plots
def plot_type(plot_type):
    if plot_type == "two_panel_ratio":
        fig, ax  = plt.subplots(2,1,
                              figsize=(7.75, 5.92), gridspec_kw={"height_ratios": [.75,.25], 
                                                               "hspace":0.1, "left":0.1, "bottom":0.11})
    elif plot_type == "three_panel_ratio":
        fig, ax = plt.subplots(3,1,
                              figsize=(7.75, 7.4), gridspec_kw={"height_ratios": [.6, .2, .2], 
                                                           "hspace":0.07, "left":0.098, "bottom":0.09})
    elif plot_type == "one_panel_wide":
        fig, ax = plt.subplots(1,1,
                              figsize=(7.75, 4.43), gridspec_kw={"hspace":0.1, "left":0.1, "bottom":0.13})
    elif plot_type == "one_panel_square":
        fig, ax = plt.subplots(1,1,
                              figsize=(7.75, 5.92), gridspec_kw={"hspace":0.1, "left":0.098, "bottom":0.11})
    else:
        raise ValueError(f"Plot type {plot_type} not recognized")
        
    return fig, ax


def drawATLASlabel(ax, yr, region, internal=True):
    #My baseline aspect (w/h)
    base_aspect = 1.81
    ex = ax.get_window_extent()
    h_base = ex.width*1/(base_aspect)
    h_def = h_base/ex.height

    ax.text(0.05, 0.9, 'ATLAS', transform=ax.transAxes,
            verticalalignment='center', horizontalalignment='left',
            fontsize=23, fontweight='bold', style='italic')
    ax.text(0.05+0.18, 0.9, 'Internal', transform=ax.transAxes,
            verticalalignment='center', horizontalalignment='left',
            fontsize=23)

    ax.text(0.05, 0.9-0.09*h_def, '$\\sqrt{s} = 13$ TeV, 20%s %.1f fb$^{-1}$' % (yr,get_lumi(yr)),
            transform=ax.transAxes,
            verticalalignment='center', horizontalalignment='left',
            fontsize=14)

    ax.text(0.05, 0.9-0.17*h_def, f'Resolved {region} Region', transform=ax.transAxes,
            verticalalignment='center', horizontalalignment='left',
            fontsize=14)
    
def drawNNTtag(ax, NNT_tag):
    ax.text(1, 1.025, NNT_tag, transform=ax.transAxes,
            verticalalignment='center', horizontalalignment='right',
            fontsize=12)

def get_xlabel(col):
    x_label_dict = {'pT_1': 'HC Jet 1 $p_{T}$ [GeV]', 'pT_2': 'HC Jet 2 $p_{T}$ [GeV]', 'pT_3': 'HC Jet 3 $p_{T}$ [GeV]', 'pT_4': 'HC Jet 4 $p_{T}$ [GeV]',
                'eta_1': 'HC Jet 1 $\\eta$', 'eta_2': 'HC Jet 2 $\\eta$', 'eta_3': 'HC Jet 3 $\\eta$', 'eta_4': 'HC Jet 4 $\\eta$',
                'pT_4_log': 'HC Jet 4 log($p_{T}$)', 'pT_2_log': 'HC Jet 2 log($p_{T}$)', 'eta_i': '<|HC $\\eta$|>',
                'dRjj_1': '$\\Delta R_{jj}$ Close', 'dRjj_2': '$\\Delta R_{jj}$ Not Close', 'njets' : 'Number of jets', 'm_hh' : '$m_{HH}$ [GeV]',
                'pt_hh': 'HH $p_{T}$ [GeV]', 'pt_hh_log': 'HH $log(p_{T})$', 'm_h1' : '$m_{H}^{lead}$ [GeV]', 'm_h2' : '$m_{H}^{subl}$ [GeV]',
                'X_wt_tag' : 'X$_{Wt}$', 'pT_h1' : 'Leading HC $p_{T}$ [GeV]', 'pT_h2' : 'Subleading HC $p_{T}$ [GeV]',
                'dEta_hh': '$\\Delta \\eta_{HH}$', 'dR_hh' : '$\\Delta R_{HH}$', 'dPhi_h1': 'Lead HC $\\Delta \\phi_{jj}$', 'dPhi_h2': 'Sublead HC $\\Delta \\phi_{jj}$',
                'pairing_score_1': 'Pairing Score 1', 'pairing_score_2' : 'Pairing Score 2', 'm_hh_cor': '$m_{HH}$ (corrected) [GeV]'}
    return x_label_dict[col]

def handle_axes(ax_list, bins, col, main_ylim=[]):   
    choices = np.array([0.1,0.2,0.5,1,5,10,20,50,100])
    step=0
    total_range = bins[-1]-bins[0]
    base_step=choices[np.where(total_range/choices>=5)[0][-1]]
    
    for i, ax in enumerate(ax_list):
        ax.tick_params(direction='in', which='both', right=True, top=True,
                  labelsize=14)

        ax.xaxis.set_major_locator(MultipleLocator(base=base_step))
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        ax.xaxis.set_minor_locator(AutoMinorLocator())
        if i < len(ax_list)-1:
            ax.xaxis.set_ticklabels([])
        else:
            ax.set_xlabel(get_xlabel(col),  horizontalalignment='right', x=1.0, fontsize=18)
         
        if i > 0:
            for label in ax.yaxis.get_majorticklabels():
                label.set_x(-0.02)
                
        if i == 0 and len(main_ylim)>0:
            ax.set_ylim(main_ylim)
            
        ax.set_xlim(bins[0], bins[-1])

def mid(bins):
    return np.array([bins[i]+(bins[i+1]-bins[i])/2. for i in range(len(bins)-1)])
def widths(bins):
    return np.array([bins[i+1]-bins[i] for i in range(len(bins)-1)])

def draw_background_hist(ax, arr, col, bins, norm, yr, norm_IQR=-1,norm_VR=-1, 
                         target_reg='4b', 
                         useVR=False, normalize=False, extra_sel='', 
                         bootstrap=False, poisson=True, syst=False, label='',
                         bottom=None, fc='yellow', ec='black', alpha=1):
    
    sel = (arr['ntag']==2)
    var = arr[sel][col]
    w_arr = arr[sel][f'NN_d24_weight_bstrap_med_{yr}']*norm
    nback, _, _ = ax.hist(arr[sel][col].values, 
                   weights= w_arr.values, bins=bins,
                   histtype='stepfilled', label=label, ec=ec, fc=fc, zorder=0, alpha=alpha)
    bs_err = 0
    if bootstrap:
        bs_err = calc_bstrap(arr[sel], col, yr, norm, norm_IQR, bins)
    
    poiss_err = 0
    if poisson:
        poiss_err = poisson_err(var, bins, weights=w_arr)
    
    syst_err = 0
    if syst:
        nom, lowVRw, highVRw, lowVRi, highVRi =HTsysts(arr[sel], norm, norm_VR, yr, bins, col)
        
        syst_err = np.sqrt((lowVRw-nom)**2 +(highVRw-nom)**2)
    
    tot_err = np.sqrt(bs_err**2 + poiss_err**2 + syst_err**2)
    
    if syst and not bootstrap and not poisson:
        err_label = 'Syst. Error'
    elif (syst and bootstrap) or (syst and poisson):
        err_label = 'Stat. + Syst. Error'
    else:
        err_label = 'Stat. Error'
        
    if bootstrap or poisson or syst:
         ax.fill_between(bins, 
                         np.append(nback-tot_err,0), 
                         np.append(nback+tot_err,0), hatch='\\\\\\\\',
                         facecolor="None", edgecolor='dimgrey', linewidth=0, step='post', 
                         zorder=1, label=err_label)
            
    return nback, tot_err

def draw_data_hist(ax, arr, col, bins, target_reg='4b', 
                  normalize=False, extra_sel='', c='k', label=''):
    
    sel = (arr['ntag']>=4)
    var = arr[sel][col]
    ndat, _ = np.histogram(arr[sel][col], bins=bins)
    
    ax.errorbar(mid(bins), ndat, yerr=np.sqrt(ndat), xerr=widths(bins)/2, fmt='o',
                ms=4, c=c, label=label)
    return ndat

def draw_ratio(ax, ndat, nback, tot_err, bins, ylabel='', ybounds=[0.8,1.2], 
               center=1, c='k', doPulls=False):
    ax.set_ylim(ybounds)
    
    if doPulls:
        ax.scatter(mid(bins), run_pulls(ndat, nback, tot_err), s=14, c=c)
        ax.plot([bins[0], bins[-1]], [0,0], 'k')
        ax.plot([bins[0], bins[-1]], [1,1], '--k')
        ax.plot([bins[0], bins[-1]], [-1,-1], '--k')
        ax.set_ylim(-3,3)
        ax.set_ylabel('Significance', fontsize=18)
    else:
        nback=nback+1e-5
        ax.errorbar(mid(bins), ndat/nback, yerr=np.sqrt(ndat)/nback, xerr=widths(bins)/2, fmt='o',
                    ms=4, c=c)
        ax.fill_between(bins, 
                        np.append((nback-tot_err)/nback,0), 
                        np.append((nback+tot_err)/nback,0), hatch='\\\\\\\\',
                        facecolor="None", edgecolor='dimgrey', linewidth=0, step='post', 
                        zorder=1)
        ax.plot([bins[0], bins[-1]], [center]*2, c='k')
        if len(ylabel)>0:
            ax.set_ylabel(ylabel, fontsize=18)
