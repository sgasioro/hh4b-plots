#!/usr/bin/env python3

import uproot
import argparse
from hh4b_plots import *

def dPhi(phi0, phi1):
    return np.pi - abs(abs(phi0-phi1) - np.pi)

def dR_hh(arr):
    return np.sqrt(dPhi(arr['phi_h1'], arr['phi_h2'])**2+(arr['eta_h1']-arr['eta_h2'])**2)

def get_wlab(target_reg):
    lab_dict = { '3b' : 'd23', '3b1l': 'd231l', '3b1f': 'd231f', '4b': 'd24'}
    return lab_dict[target_reg]

def sorted_jet_pTs(full_data, with_idxs = False):
    pt_cols = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']
    if with_idxs:
        sorted_pT_idxs = np.argsort(-full_data[pt_cols], axis=1)
        sorted_pTs = np.take_along_axis(full_data[pt_cols].values, sorted_pT_idxs.values, axis=1)
        return sorted_pTs, sorted_pT_idxs
    else:
        sorted_pTs = np.sort(full_data[pt_cols], axis=1)[:, ::-1]
        return sorted_pTs

def add_extras(arr, doLog=[], verbose=True):
    arr['dPhi_h1'] = dPhi(arr['phi_h1_j1'], arr['phi_h1_j2'])
    arr['dPhi_h2'] = dPhi(arr['phi_h2_j1'], arr['phi_h2_j2'])
    arr['dR_hh'] = dR_hh(arr)

    sorted_pTs, sorted_pT_idxs = sorted_jet_pTs(arr, with_idxs = True)
    arr['pT_1'] = sorted_pTs[:, 0]
    arr['pT_3'] = sorted_pTs[:, 2]

    eta_cols = ['eta_h1_j1', 'eta_h1_j2', 'eta_h2_j1', 'eta_h2_j2']
    sorted_etas = np.take_along_axis(arr[eta_cols].values, sorted_pT_idxs.values, axis=1)
    for idx in range(4):
        arr[f'eta_{idx+1}'] = sorted_etas[:, idx]

    if doLog:
        if verbose:
            print("Taking logs of", doLog)
        for col in doLog:
            arr.insert(0, col+'_log', np.log(arr[col]), False)

    return arr


def main(data_file, yr, NNT_tag, tree):

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
               'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'pt_hh',
               'pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2',
               'eta_h1_j1', 'eta_h1_j2', 'eta_h2_j1', 'eta_h2_j2',
               'phi_h1_j1', 'phi_h1_j2', 'phi_h2_j1', 'phi_h2_j2', 'phi_h1', 'phi_h2', 'eta_h1', 'eta_h2', 'pT_h1', 'pT_h2',
               'pairing_score_1', 'pairing_score_2', 'm_hh', 'm_hh_cor', 'dEta_hh',
               f'NN_d24_weight_bstrap_med_{yr}', f'NN_d24_weight_VRderiv_bstrap_med_{yr}',
               f'NN_d24_weight_bstrap_IQR_{yr}', 'X_wt_tag']

    f_in = uproot.open(data_file)
    full_data = f_in[tree].pandas.df(columns)

    med_norm_nom = f_in[f'NN_norm_bstrap_med_{yr}']._fVal
    med_norm_nom_VR = f_in[f'NN_norm_VRderiv_bstrap_med_{yr}']._fVal
    norm_IQR_nom = f_in[f'NN_norm_bstrap_IQR_{yr}']._fVal

    columns_to_plot =  ['pT_1', 'pT_2', 'pT_3', 'pT_4', 'eta_1', 'eta_2', 'eta_3', 'eta_4',
                        'eta_i', 'dRjj_1', 'dRjj_2', 'pt_hh', 'njets','m_hh',
                        'm_hh_cor', 'm_h1', 'm_h2', 'pT_h1', 'pT_h2',
                        'pT_4_log', 'pT_2_log', 'pt_hh_log', 'X_wt_tag', 'dEta_hh', 'dPhi_h1',
                        'dPhi_h2', 'dR_hh', 'pairing_score_1', 'pairing_score_2']

    full_data = add_extras(full_data, doLog=['pT_4', 'pT_2', 'pt_hh'])

    for col in columns_to_plot:
        print(col)
        xlim = np.percentile(full_data[col], [0.01, 99.99])
        width = xlim[1]-xlim[0]
        xlim[0] -= width*0.1
        xlim[1] += width*0.1

        n_bins = 50
        if col == 'njets':
            n_bins = 8
            xlim = [3.5,11.5]
        if col == "m_hh":
            n_bins= 39
            xlim = [225,1200]
        if col == "m_hh_cor":
            n_bins = 84
            xlim = [250, 1450]
        if col == 'X_wt_tag':
            xlim[0] = 1.5

        bins = np.linspace(xlim[0], xlim[1], n_bins+1)

        fig, ax = plot_type('two_panel_ratio')
        nback, tot_err = draw_background_hist(ax[0], full_data, col, bins, 
                                              med_norm_nom, yr, norm_IQR=norm_IQR_nom, 
                                              bootstrap=True, label='Reweighted 2b Data', 
                                              fc='hh:darkyellow', ec='black', alpha=0.9)

        ndat = draw_data_hist(ax[0], full_data, col, bins, label='4b Data')

        draw_ratio(ax[1], ndat, nback, tot_err, bins, ylabel='4b / 2b', ybounds=[0.5, 1.5])

        handle_axes(ax, bins, col, main_ylim =[0, (nback*1.6).max()])

        tree_label = {'sig' : 'Signal', 'validation':'Validation', 'control': 'Control'}
        drawATLASlabel(ax[0], yr, tree_label[tree])
        drawNNTtag(ax[0], NNT_tag)

        ax[0].legend(loc='upper right', fontsize=18)

        output_fname = (NNT_tag+'_'+col+'_'+tree_label[tree]+'_NN_'+yr_short+'.pdf').replace('_', '-')
        plt.savefig(output_fname)
        plt.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--data", dest="data_file", default="",
                        help="Input data filename")
    parser.add_argument("-y", "--year", dest="year", default="",
                        help="Year")
    parser.add_argument("--NNT-tag", dest="NNT_tag", default="",
                        help="Tag for NNT version")
    parser.add_argument("--tree", dest="tree", default="",
                        help="Tree on which to make plots")

    args = parser.parse_args()

    #Processing for year input
    year =''
    yr_short=''
    year_in = args.year
    if len(year_in) == 4:
        year=year_in
        yr_short=year_in[2:]
    elif len(year_in) == 2:
        year='20'+year_in
        yr_short=year_in
    else:
        raise ValueError(f"Invalid year format: {year_in}")

    main(args.data_file, yr_short, args.NNT_tag, args.tree)

