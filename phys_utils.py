import numpy as np

def get_lumi(yr):
    lumi = {'15' : 3.2, '16' : 24.6, '17': 43.65, '18': 57.7, 'all':126}
    return lumi[yr]

def poisson_err(var, bins, weights=[]):
    if len(weights)==0:
        weights = np.ones(len(var))
    return np.sqrt(np.histogram(var, weights=weights**2, bins=bins)[0])

def run_pulls(h_obs, h_exp, h_exp_err):
    h_pull = np.zeros(len(h_obs))
    for i in range(len(h_obs)):
        nbObs = h_obs[i]
        nbExp = h_exp[i]
        nbExpEr = h_exp_err[i]
        if nbExp == 0 or nbObs == 0:
            continue
        #significance calculated from https://cds.cern.ch/record/2643488
        # relabel variables to match CDS formula
        factor1 = nbObs*np.log( (nbObs*(nbExp+nbExpEr**2))/(nbExp**2+nbObs*nbExpEr**2) )
        factor2 = (nbExp**2/nbExpEr**2)*np.log( 1 + (nbExpEr**2*(nbObs-nbExp))/(nbExp*(nbExp+nbExpEr**2)) )
        if nbObs < nbExp:
            pull  = -np.sqrt(2*(factor1 - factor2))
        else:
            pull  = np.sqrt(2*(factor1 - factor2))
        h_pull[i] = pull
    return h_pull

def calc_bstrap(arr, col, yr, norm, norm_IQR, bins):
    n2,_ = np.histogram(arr[arr['ntag']==2][col],
                        weights=arr[arr['ntag']==2]['NN_d24_weight_bstrap_med_%s'%yr]*norm,
                        bins=bins)

    n_IQRup,_ = np.histogram(arr[arr['ntag']==2][col],
                              weights=(arr[arr['ntag']==2]['NN_d24_weight_bstrap_med_%s'%yr]
                                       +arr[arr['ntag']==2]['NN_d24_weight_bstrap_IQR_%s'%yr]/2.),
                              bins=bins)

    return abs(n_IQRup*np.sum(n2)/np.sum(n_IQRup)+n2*norm_IQR/2.-n2)

def HTsysts(arr, norm, norm_VR, yr, bins, col= 'm_hh', var='HT', cutoff=300):
    if var in arr.keys():
        var_arr = arr[var]
    else:
        if var == 'HT':
            var_arr = arr['pT_h1_j1'] + arr['pT_h1_j2'] + arr['pT_h2_j1'] + arr['pT_h2_j2']
        else:
            print("Not found!")
            return 0

    n_back_low, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr< cutoff)][col], bins=bins,
                            weights=arr[(arr['ntag']==2)& (var_arr< cutoff)]['NN_d24_weight_bstrap_med_%s'%yr]*norm)

    n_back_high, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr >= cutoff)][col], bins=bins,
                        weights=arr[(arr['ntag']==2)& (var_arr >= cutoff)]['NN_d24_weight_bstrap_med_%s'%yr]*norm)

    n_back_low_VR, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr< cutoff)][col], bins=bins,
                            weights=arr[(arr['ntag']==2)& (var_arr< cutoff)]['NN_d24_weight_VRderiv_bstrap_med_%s'%yr]*norm_VR)

    n_back_high_VR, _ = np.histogram(arr[(arr['ntag']==2) & (var_arr >= cutoff)][col], bins=bins,
                        weights=arr[(arr['ntag']==2)& (var_arr >= cutoff)]['NN_d24_weight_VRderiv_bstrap_med_%s'%yr]*norm_VR)


    lowVRw = n_back_low_VR + n_back_high
    highVRw = n_back_low + n_back_high_VR
    nom = n_back_low + n_back_high

    lowVRi = 2*nom - lowVRw
    highVRi = 2*nom - highVRw

    return nom, lowVRw, highVRw, lowVRi, highVRi
